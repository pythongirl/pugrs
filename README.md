# Pug.rs
![pipeline](https://gitlab.com/pythondude325/pugrs/badges/master/pipeline.svg)

A rust implementation of Pug.js

## Todo
- Add filter plugins. Use dylib or feed in as HashMap<String, Fn(String) -> String>
- Use embeded scripting language (Gluon, RLua, Rhai) as the replacement for JS in Pug.rs
- Add more options for indentation (current is tabs or 4 spaces)

## Relevant Links
- [Pug.js reference implementation](https://pugjs.org)
- [Nom Docs](https://docs.rs/nom)
- [Reference implementation of indentation parser in nom](https://github.com/Geal/nom/issues/327)
- [Nom Choosing a Combinator](https://github.com/Geal/nom/blob/master/doc/choosing_a_combinator.md)
- [Gluon (scripting lang)](https://gluon-lang.org/doc/nightly/book/index.html)
