#[macro_use]
extern crate nom;
#[macro_use]
extern crate nom_trace;

declare_trace!();

use nom::types::CompleteStr;

mod line_contents;
use line_contents::{parse_line_content, LineContents};

#[derive(Debug)]
enum Indentation {
    Tabs(usize),
    Spaces(usize),
}

#[derive(Debug)]
struct Line {
    indentation: Option<Indentation>,
    contents: LineContents,
}

named!(parse_indentation<CompleteStr, Option<Indentation> >,
    opt!(
        alt!(
            many1_count!(tag!("\t")) => { |l| Indentation::Tabs(l) } |
            many1_count!(tag!(" ")) => { |l| Indentation::Spaces(l) }
        )
    )
);

named!(parse_line<CompleteStr, Line>,
    do_parse!(
        indentation: parse_indentation >>
        contents: parse_line_content >>
        (Line { indentation, contents })
    )
);

fn main() {
    println!("{:?}", parse_line(CompleteStr("div")));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_line(s: &str) {
        println!(
            "Test case: {}, Result: {:?}",
            s,
            parse_line(CompleteStr(&s))
        );
        reset_trace!();
    }

    #[allow(dead_code)]
    fn trace_test_line(s: &str) {
        println!(
            "Test case: {}, Result: {:?}",
            s,
            parse_line(CompleteStr(&s))
        );
        print_trace!();
        reset_trace!();
    }

    #[test]
    fn parse_lines() {
        test_line("32(hi)");
        test_line("p(data-thing)");
        test_line("p( data-thing = thing )");
        test_line("p(data-thing=\"thing\")");
        test_line("p(data1 data2)");
        test_line("32()");
        test_line("32");
        test_line("// buffered");
        test_line("//- unbuffered");
        test_line("doctype ");
        test_line("doctype html");
        test_line("doctype xml");
        test_line(":filter-name");
        test_line(":fliter(with options)");
        test_line("- This is unbuffered code");
        test_line("=This is buffered code");
        test_line("if expr1");
        test_line("else if expr2");
        test_line("else expr3");
        test_line("unless expr4");
        test_line("mixin name");
        test_line("mixin name2()");
        test_line("mixin name3(arg)");
        test_line("mixin name4(arg1, arg2)");
        test_line("+name1");
        test_line("+name2()");
        test_line("+name3(arg)");
        test_line("+name4(arg1, arg2)");
    }
}
